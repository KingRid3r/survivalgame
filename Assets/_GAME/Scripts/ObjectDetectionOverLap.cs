﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDetectionOverLap : MonoBehaviour
{
    [SerializeField] private CapsuleCollider m_Collider = null;

    [SerializeField] private float m_Detectionrange = 5f;

    [SerializeField] private LayerMask m_DetectedLayers = ~0;

    [SerializeField] private LayerMask m_SightLayers = ~0;

    private void Awake()
    {
        if (m_Collider == null)
        {
            m_Collider = GetComponentInParent<CapsuleCollider>();
        }
    }

    private void Update()
    {
        Collider[] objs = Physics.OverlapSphere(transform.position, m_Detectionrange, m_DetectedLayers);
        foreach (Collider other in objs)
        {
            Debug.Log("Detected: " + other.name);
            Vector3 toOther = other.transform.position - transform.position;
            RaycastHit[] hits = Physics.SphereCastAll(transform.position, m_Collider.radius, toOther, toOther.magnitude,
                m_SightLayers);
            foreach (RaycastHit hit in hits)
            {
                if (hit.collider == other) {
                    Debug.Log("Seeing: " + other.name);
                    continue;
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, m_Detectionrange);
    }
}