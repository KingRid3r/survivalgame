﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoBulle : MonoBehaviour
{
    //public static Array Inputs = new 
    // Start is called before the first frame update
    [SerializeField]
    private RawImage InfoImage;
    [SerializeField]
    private Text InfoText;
    void Start()
    {
        UnDisplay();
    }

    public void SetInfobulleContent(string button, string info)
    {
        InfoImage.texture = InputsTextureGetter.getTexture(button);
        InfoText.text = info;
    }

    public void Display()
    {
        gameObject.SetActive(true);
    }
    
    public void UnDisplay()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
