﻿using UnityEngine;

public class InputsTextureGetter
{
    public static Texture getTexture(string button)
    {
       return Resources.Load<Texture2D>("IU/Inputs/" + button);
    }
}