﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarController : MonoBehaviour
{
    
    private Slider slider;

    public void Awake()
    {
        if (!slider)
        {
            slider = GetComponent<Slider>();
        }
    }

    public void SetMax(int health)
    {
        slider.maxValue = health;
        Set(health);
    }

    public void Set(int health)
    {
        slider.value = health;
    }
    
}
