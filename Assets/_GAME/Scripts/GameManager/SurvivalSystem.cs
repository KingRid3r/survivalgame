﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurvivalSystem : MonoBehaviour
{
    [SerializeField]
    private NetworkPlayerController player;


    [SerializeField]
    private static float lifeLawTime = 1;
    [SerializeField]
    private static float lifeLawRepeatRate = 1F;


    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("ApplyLifeLaw", lifeLawTime, lifeLawRepeatRate);
    }

    void ApplyLifeLaw()
    {
        if (!player && GameObject.FindGameObjectWithTag("MainPlayer"))
        {
            player = GameObject.FindGameObjectWithTag("MainPlayer").GetComponent<NetworkPlayerController>();
        }
        if (player)
        {
            player.hurtHunger(0.10);
            player.hurtWater(0.5);
            if (player.getCurrentHunger() == 0)
            {
                player.hurtHealth(0.5);
            }
            if (player.getCurrentWater() == 0)
            {
                player.hurtHealth(0.5);
            }

            if (player.getCurrentHealth() == 0)
            {
                this.GameOver();
            }
        }
    }

    private void GameOver()
    { ;
        Debug.Log("GameOver");
    }

    // Update is called once per frame
    void Update()
    {
    }
}
