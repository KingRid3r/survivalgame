﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private BarController healthBar;
    [SerializeField]
    private BarController hungerBar;
    [SerializeField]
    private BarController waterBar;
    
    [SerializeField]
    private Animator animator = null;
    
    [SerializeField] 
    private InputActionAsset Controls;

    [SerializeField] private Transform PlayerCamera;

    [SerializeField] 
    private float speed = 6f;

    private float beforeFireSpeedState;

    private Vector2 movement = Vector2.zero;


    public int maxHealth = 100;
    public int maxHunger = 100;
    public int maxWater = 100;

    private double currentHealth;
    private double currentHunger;
    private double currentWater;

    public double getCurrentHealth()
    {
        return this.currentHealth;
    }
    public double getCurrentHunger()
    {
        return this.currentHunger;
    }
    public double getCurrentWater()
    {
        return this.currentWater;
    }


    private void Start()
    {
        
        AwakeBar();
        
        //GetAnimator
        if (!animator)
        {
            animator = GetComponent<Animator>();
        }

        AwakeInputs();
    }

    private void AwakeBar()
    {
        currentHealth = maxHealth;
        currentHunger = maxHunger;
        currentWater = maxWater;
        healthBar.SetMax(maxHealth);
        hungerBar.SetMax(maxHunger);
        waterBar.SetMax(maxWater);   
    }

    private void AwakeInputs()
    {
        InputActionMap playerMap = Controls.FindActionMap("Player");
        
        InputAction fireAction = playerMap.FindAction("Shoot");

        InputAction moveAction = playerMap.FindAction("Move");

        InputAction runAction = playerMap.FindAction("Run");
        
        fireAction.started += Fire;
        fireAction.canceled += (ctx) => { UnBlockMouvement(); };


        moveAction.performed += (ctx) => { movement = ctx.ReadValue<Vector2>(); };
        moveAction.canceled += (ctx) => { movement = Vector2.zero; };
        

        runAction.performed += (ctx) => { speed = 20f; };
        runAction.canceled += (ctx) => { speed = 6f; };

        Controls.Enable();
    }
    

    private void Update()
    {
        Move(movement, Time.deltaTime);
        animator.SetFloat("speed", movement == Vector2.zero ? 0f: speed);
    }

    private void Move(Vector2 direction, float deltaTime)
    {
        if (direction == Vector2.zero) {
            return;
        }
        
        direction.Normalize();
        Vector3 forward = PlayerCamera.transform.TransformDirection(Vector3.forward);
        Vector3 right = PlayerCamera.transform.TransformDirection(Vector3.right);
        float curSpeedY = speed * direction.x;
        float curSpeedX = speed * direction.y ;
        Vector3 moveDirection = (forward * curSpeedX) + (right * curSpeedY);
        moveDirection = new Vector3(moveDirection.x, 0, moveDirection.z);
        
        transform.position += moveDirection * deltaTime;
        transform.forward += moveDirection;

    }

    private void Fire(InputAction.CallbackContext ctx) {
        animator.SetTrigger("punch");
        this.BlockMouvement();
    }

    private void BlockMouvement()
    {
        this.beforeFireSpeedState = speed;
        speed = 0;
    }
    
    private void UnBlockMouvement()
    {
        speed = beforeFireSpeedState;
    }

}
