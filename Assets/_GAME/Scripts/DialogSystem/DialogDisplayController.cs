﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class DialogDisplayController : MonoBehaviour
{
    [SerializeField] 
    private InputActionAsset Controls;
    
    private Text currentText ;
    private RawImage dialogBox;

    private Queue<String> dialogQueue = new Queue<string>();
    
    // Start is called before the first frame update
    void Awake()
    {
        InputActionMap playerMap = Controls.FindActionMap("Player");
        InputAction passDialogAction = playerMap.FindAction("PassDialog");
        
        passDialogAction.started += (ctx) => { DequeueDialog(); };
        
        currentText = GetComponentInChildren<Text> ();
        dialogBox = GetComponent<RawImage>();
        Hide();
    }

    // Update is called once per frame
    void Update()
    {
        if (dialogQueue.Count > 0)
        {
            Show();
            DisplayText(dialogQueue.First());
        }
        else
        {
            Hide();
        }
    }

    public void AddDialogText(String text)
    {
        if (text.Length > 430)
        {
            throw new Exception("Max string length is 430");
        }
        else
        {
            dialogQueue.Enqueue(text);
        }
    }

    private void DequeueDialog()
    {
        if (dialogQueue.Count > 0)
        {
            dialogQueue.Dequeue();
        }
    }

    private void DisplayText(string text)
    {
        currentText.text = text;
    }

    private void Hide()
    {
        currentText.enabled = false;
        dialogBox.enabled = false;
        for(int i=0; i< dialogBox.transform.childCount; i++)
        {
            var child = dialogBox.transform.GetChild(i).gameObject;
            if(child != null)
                child.SetActive(false);
        }
    }
    
    private void Show()
    {
        currentText.enabled = true;
        dialogBox.enabled = true;
        for(int i=0; i< dialogBox.transform.childCount; i++)
        {
            var child = dialogBox.transform.GetChild(i).gameObject;
            if(child != null)
                child.SetActive(true);
        }

    }
}
