﻿
using UnityEngine;
using UnityEngine.InputSystem;

public class PNGController: MonoBehaviour
{
    [SerializeField] 
    private DialogDisplayController dialogController;
    
    [SerializeField] 
    private InfoBulle infoBulle;
    
    [SerializeField] 
    private InputActionAsset Controls;
    
    private bool waitPressKey = false;

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.CompareTag("MainPlayer"))
        {
            this.infoBulle.SetInfobulleContent("Y", "Pour parler");
            this.infoBulle.Display();
            waitPressKey = true;
        }
    }
    
    private void Awake() {
        
        InputActionMap playerMap = Controls.FindActionMap("Player");
        InputAction toggleInvAction = playerMap.FindAction("Talk");
        
        toggleInvAction.started += (ctx) => { DisplayDialog(); };
    }

    private void DisplayDialog()
    {
        dialogController.AddDialogText("Salut mec, on est dans la sauce ! Les boules jaunes ont pris le contrôle du monde et nous ont enfermé dans ce jeu vidéo complètement beggé ça race !");
        dialogController.AddDialogText("Je te conseil d'aller te ravitailler dans la tente mais je te previens Je n’ai jamais réussi à le finir !");
        this.infoBulle.UnDisplay();
        waitPressKey = false;
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {   
            // Stop display info
            infoBulle.UnDisplay();
            waitPressKey = false;
            // Then if press specific key then destroy after adding in inventory
        }
    }
    
}
