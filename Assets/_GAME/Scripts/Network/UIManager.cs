﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] 
    private InputActionAsset Controls;
    
    public static UIManager instance;

    public GameObject startMenu;
    public InputField usernameField;
    public InputField IpField;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            Time.timeScale = 0f;
            usernameField.text = "Player";
            
            InputActionMap networkMap = Controls.FindActionMap("NetworkControl");
            InputAction enterOnlineGame = networkMap.FindAction("ConnectToServer");
        
            enterOnlineGame.started += (ctx) =>
            {
                ConnectToServer();
                networkMap.Disable();
            };
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }
    
    

    /// <summary>Attempts to connect to the server.</summary>
    public void ConnectToServer()
    {
        
        startMenu.SetActive(false);
        usernameField.interactable = false;
        IpField.interactable = false;

        Client.instance.ConnectToServer();
        Time.timeScale = 1f;
    }
}
