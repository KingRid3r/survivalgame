﻿using UnityEngine;
using UnityEngine.InputSystem;

public class NetworkPlayerController : MonoBehaviour
{
    [SerializeField]
    private BarController healthBar;
    [SerializeField]
    private BarController hungerBar;
    [SerializeField]
    private BarController waterBar;
    
    [SerializeField]
    private Animator animator = null;
    
    [SerializeField] 
    private InputActionAsset Controls;

    [SerializeField] 
    private GameObject PlayerCamera;

    [SerializeField] 
    private float speed = 6f;

    private Vector2 movement = Vector2.zero;
    public Transform hitZone;
    
    public int maxHealth = 100;
    public int maxHunger = 100;
    public int maxWater = 100;

    private double currentHealth;
    private double currentHunger;
    private double currentWater;

    public double getCurrentHealth()
    {
        return this.currentHealth;
    }
    public double getCurrentHunger()
    {
        return this.currentHunger;
    }
    public double getCurrentWater()
    {
        return this.currentWater;
    }


    private void Start()
    {
        AwakeBar();
        
        PlayerCamera = GameObject.FindWithTag("MainCamera");
        PlayerCamera.GetComponent<PlayerFollow>().PlayerTransform = gameObject.transform;
        //GetAnimator
        if (!animator)
        {
            animator = GetComponent<Animator>();
        }
        
        

        AwakeInputs();
    }
    
    private void AwakeBar()
    {
        currentHealth = maxHealth;
        currentHunger = maxHunger;
        currentWater = maxWater;
        healthBar = GameObject.FindGameObjectWithTag("HealthBar").GetComponent<BarController>();
        hungerBar = GameObject.FindGameObjectWithTag("HungerBar").GetComponent<BarController>();
        waterBar = GameObject.FindGameObjectWithTag("WaterBar").GetComponent<BarController>();
        healthBar.SetMax(maxHealth);
        hungerBar.SetMax(maxHunger);
        waterBar.SetMax(maxWater);   
    }
    
    private void AwakeInputs()
    {
        InputActionMap playerMap = Controls.FindActionMap("Player");
        
        InputAction fireAction = playerMap.FindAction("Shoot");

        InputAction moveAction = playerMap.FindAction("Move");

        InputAction runAction = playerMap.FindAction("Run");

        fireAction.started += Fire;
        
        moveAction.performed += (ctx) =>
        {
            movement = ctx.ReadValue<Vector2>();
            
        };
        moveAction.canceled += (ctx) => { movement = Vector2.zero; };

        runAction.performed += (ctx) => { speed = 20f; };
        runAction.canceled += (ctx) => { speed = 6f; };
        
        Controls.Enable();
    }
    
    private void Update()
    {
        Move(movement, Time.deltaTime);
        animator.SetFloat("speed", movement == Vector2.zero ? 0f: speed);
    }

    private void Move(Vector2 direction, float deltaTime)
    {
        if (direction == Vector2.zero) {
            return;
        }
        
        direction.Normalize();
        Vector3 forward = PlayerCamera.transform.TransformDirection(Vector3.forward);
        Vector3 right = PlayerCamera.transform.TransformDirection(Vector3.right);
        float curSpeedY = speed * direction.x;
        float curSpeedX = speed * direction.y ;
        Vector3 moveDirection = (forward * curSpeedX) + (right * curSpeedY);
        moveDirection = new Vector3(moveDirection.x, 0, moveDirection.z);
        
        transform.position += moveDirection * deltaTime;
        transform.forward += moveDirection;

    }
    
    private void Fire(InputAction.CallbackContext ctx) {
        animator.SetTrigger("punch");
        SendAttackToServer();
    }

    private void FixedUpdate()
    {
        SendNewPositionToServer();
    }

    /// <summary>Sends player input to the server.</summary>
    private void SendNewPositionToServer()
    {
        // bool[] _inputs = new bool[]
        // {
        //     Input.GetKey(KeyCode.W),
        //     Input.GetKey(KeyCode.S),
        //     Input.GetKey(KeyCode.A),
        //     Input.GetKey(KeyCode.D),
        //     Input.GetKey(KeyCode.Space)
        // };
        //
        // ClientSend.PlayerMovement(_inputs);
        ClientSend.PlayerMovement(gameObject.transform.position, gameObject.transform.rotation);
    }
    
    /// <summary>Sends attack of player to Server.</summary>
    private void SendAttackToServer()
    {
        ClientSend.PlayerAttack(hitZone.position);
    }
    
    public void hurtHealth(double value)
    {
        if (currentHealth - value < 0)
        {
            currentHealth = 0;
        }
        else
        {
            currentHealth = currentHealth - value;
        }
        healthBar.Set((int) currentHealth);
    }
    
    public void hurtHunger(double value)
    {
        if (currentHunger - value < 0)
        {
            currentHunger = 0;
        }
        else
        {
            currentHunger = currentHunger - value;
        }
        hungerBar.Set((int) currentHunger);
    }
    
    public void hurtWater(double value)
    {
        if (currentWater - value < 0)
        {
            currentWater = 0;
        }
        else
        {
            currentWater = currentWater - value;
        }
        waterBar.Set((int) currentWater);
    }


    public void IncreaseHealth(double value)
    {
        if (currentHealth + value > maxHealth)
        {
            currentHealth = 100;
        }
        else
        {
            currentHealth = currentHealth + value;
        }
        healthBar.Set((int) currentHealth);
    }
    
    public void IncreaseWater(double value)
    {
        if (currentWater + value > maxWater)
        {
            currentWater = 100;
        }
        else
        {
            currentWater = currentWater + value;
        }
        waterBar.Set((int) currentWater);
    }
    public void IncreaseHunger(double value)
    {
        if (currentHunger + value > maxHunger)
        {
            currentHunger = 100;
        }
        else
        {
            currentHunger = currentHunger + value;
        }
        hungerBar.Set((int) currentHunger);
    }
}
