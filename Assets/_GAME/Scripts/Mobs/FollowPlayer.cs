﻿using System.Linq;
using UnityEditor;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform target; //the enemy's target
    public int moveSpeed = 3; //move speed
    public int rotationSpeed = 3; //speed of turning
    public float range; //Range within target will be detected
    public float stop;
    public Transform myTransform; //current transform data of this enemy
    private Vector3 look;

    private void Awake()
    {
        target = GameObject.FindWithTag("Player").transform; //target the player
        myTransform = transform; //cache transform data for easy access/preformance
    }

    public void Update () {    //rotate to look at the player
        var distance = Vector3.Distance(myTransform.position, target.position);
        
        if (distance<=range){
            //look
            // myTransform.rotation = Quaternion.Slerp(myTransform.rotation,
            //     Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed*Time.deltaTime);
            //move
            
            if(distance>stop)
            {
                look.Set(target.position.x, myTransform.position.y, target.position.z);
                myTransform.position = Vector3.MoveTowards(myTransform.position, target.position, (moveSpeed * Time.deltaTime));
                myTransform.LookAt(look);
                //myTransform.position += myTransform.forward * (moveSpeed * Time.deltaTime);
            }
        }
    }
    
}
