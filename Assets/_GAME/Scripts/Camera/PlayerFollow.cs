﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerFollow : MonoBehaviour
{
    [SerializeField]
    public Transform PlayerTransform;

    private Vector3 _cameraOffset;
    
    private Vector2 CameraMovement = Vector2.zero;
    
    [SerializeField] 
    private InputActionAsset Controls;

    [SerializeField] [Range(0.01f, 1.0f)] private float SmoothFactor = 0.5f;

    [SerializeField] private bool LookAtPlayer = false;

    [SerializeField] private bool RotateAroundPlayer = true;

    [SerializeField] private float RotationsSpeed = 2.0f;

    void Start()
    {
        _cameraOffset = transform.position - PlayerTransform.position;

        InputActionMap playerMap = Controls.FindActionMap("Player");
        InputAction cameraMoveAction = playerMap.FindAction("View");


        cameraMoveAction.performed += (ctx) => { CameraMovement = ctx.ReadValue<Vector2>(); };
        cameraMoveAction.canceled += (ctx) => { CameraMovement = Vector2.zero; };
    }

    void LateUpdate()
    {
        if (RotateAroundPlayer)
        {
            Quaternion camTurnAngle = Quaternion.AngleAxis(CameraMovement.x * RotationsSpeed, Vector3.up);
            _cameraOffset = camTurnAngle * _cameraOffset;
        }
        
        Vector3 newPos = PlayerTransform.position + _cameraOffset;
        transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);

        if (LookAtPlayer || RotateAroundPlayer)
        {
            transform.LookAt(PlayerTransform);
        }
    }
}
