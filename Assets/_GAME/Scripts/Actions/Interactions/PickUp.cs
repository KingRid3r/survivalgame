﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PickUp : MonoBehaviour
{
    public InfoBulle infobulle;
    public GameObject playerInventory;
    [SerializeField] 
    private InputActionAsset Controls;

    private bool takable =  false;
    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.CompareTag("MainPlayer"))
        {
            // First print that we can interact
            this.infobulle.SetInfobulleContent("X", "Pour prendre");
            this.infobulle.Display();
            // Then if press specific key then destroy after adding in inventory
            takable = true;
        }
    }
    
    
    private void Awake() {
        
        InputActionMap playerMap = Controls.FindActionMap("Player");
        InputAction toggleInvAction = playerMap.FindAction("PickUp");
        
        toggleInvAction.started += (ctx) => { StartPickUp(); };
    }

    private void StartPickUp()
    {
        if (takable)
        {
            playerInventory.GetComponent<InventoryHandler>().addItem(gameObject);
            this.infobulle.UnDisplay();
            takable = false;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {   
            // Stop display info
            infobulle.UnDisplay();
            takable = false;
            // Then if press specific key then destroy after adding in inventory
        }
    }
}
