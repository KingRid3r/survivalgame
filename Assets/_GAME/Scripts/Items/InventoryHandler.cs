﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class InventoryHandler : MonoBehaviour
{
    private bool isInventory = false;
    
    public Canvas canvas;
    public InventoryItem space1;
    public InventoryItem space2;
    public InventoryItem space3;
    public InventoryItem space4;
    public InventoryItem space5;
    
    

    [SerializeField]
    private Transform selector;
    
    [SerializeField] 
    private InputActionAsset Controls;

    // Update is called once per frame

    private void Awake() {
        
        InputActionMap playerMap = Controls.FindActionMap("Player");
        
        InputActionMap inventoryMap = Controls.FindActionMap("Inventory");
        InputAction moveRight = inventoryMap.FindAction("MoveRight");
        InputAction moveLeft = inventoryMap.FindAction("MoveLeft");
        InputAction use = inventoryMap.FindAction("Use");
        InputAction drop = inventoryMap.FindAction("Drop");
        
        moveRight.started += (ctx) => { MoveRight(); };
        moveLeft.started += (ctx) => { MoveLeft(); };
        
        use.started += (ctx) => { Use(); };
        drop.started += (ctx) => { Drop(); };
        
        Controls.Enable();
    }

    private void Update()
    {
        
    }

    private void Drop()
    {
        if (space1.selected)
        {
            space1.drop();
        }
        if (space2.selected)
        {
            space2.drop();
        }
        if (space3.selected)
        {
            space3.drop();
        }
        if (space4.selected)
        {
            space4.drop();
        }
        if (space5.selected)
        {
            space5.drop();
        }
    }
    
    private void Use()
    {
        if (space1.selected)
        {
            space1.actionItem();
        }
        if (space2.selected)
        {
            space2.actionItem();
        }
        if (space3.selected)
        {
            space3.actionItem();
        }
        if (space4.selected)
        {
            space4.actionItem();
        }
        if (space5.selected)
        {
            space5.actionItem();
        }
    }

    private void MoveRight()
    {
        if (space1.selected)
        {
            space1.selected = false;
            space2.selected = true;
        } else if (space2.selected)
        {
            space2.selected = false;
            space3.selected = true;
        } else if (space3.selected)
        {
            space3.selected = false;
            space4.selected = true;
        } else if (space4.selected)
        {
            space4.selected = false;
            space5.selected = true;
        } else if (space5.selected)
        {
            space5.selected = false;
            space1.selected = true;
        } else
        {
            space1.selected = true;
        }
    }
    private void MoveLeft()
    {
        Debug.Log("test");
        if (space1.selected)
        {
            space1.selected = false;
            space5.selected = true;
        } else if (space2.selected)
        {
            space2.selected = false;
            space1.selected = true;
        } else if (space3.selected)
        {
            space3.selected = false;
            space2.selected = true;
        } else if (space4.selected)
        {
            space4.selected = false;
            space3.selected = true;
        } else if (space5.selected)
        {
            space5.selected = false;
            space4.selected = true;
        }
        else
        {
            space1.selected = true;
        }
    }
    
    

    public void addItem(GameObject item)
    {
        Debug.Log(item.GetComponent<ItemObject>());
        if (space1.checkEmpty())
        {
            space1.add(item.GetComponent<ItemObject>().itemName, item);
        }
        else
        {
            if (space2.checkEmpty())
            {
                space2.add(item.GetComponent<ItemObject>().itemName, item);
            }
            else
            {
                if (space3.checkEmpty())
                {
                    space3.add(item.GetComponent<ItemObject>().itemName, item);
                }
                else
                {
                    if (space4.checkEmpty())
                    {
                        space4.add(item.GetComponent<ItemObject>().itemName, item);
                    }
                    else
                    {
                        if (space5.checkEmpty())
                        {
                            space5.add(item.GetComponent<ItemObject>().itemName, item);
                        }
                    }
                }
            }
        }
    }

    private void toggleDisplay()
    {
        canvas.GetComponent<Canvas>().enabled = !canvas.GetComponent<Canvas>().enabled;
    }
}
