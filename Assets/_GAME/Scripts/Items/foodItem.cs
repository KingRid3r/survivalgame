﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class foodItem : ItemObject
{
    void Start()
    {
        itemName = "Meat";
        itemValue = 50;
        itemDescription = "Ceci est une viande qui regenere 50 points de faim !";
        InvokeRepeating("RetievePlayer", 1, 1);

    }

    public void eat()
    {
        Debug.Log(player);
        if (player)
        {
            player.IncreaseHunger(itemValue);
        }
    }
}
