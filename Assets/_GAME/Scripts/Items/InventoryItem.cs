﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour
{
    public String itemName;
    private GameObject prefabItem;
    public static GameObject dropZone;
    
    public Boolean selected = true;
    
    [SerializeField]
    private RawImage selector;

    private RawImage ItemImage;
    
    // Start is called before the first frame update
    void Awake()
    {
        itemName = "Empty";
        ItemImage = GetComponent<RawImage>();
        ItemImage.texture = Resources.Load<Texture2D>("Items/" + itemName);
    }

    void Start()
    {
        InvokeRepeating("RetieveDropZone", 1, 1);

    }
    
    void RetieveDropZone()
    {
        if (!dropZone && GameObject.FindGameObjectWithTag("DropZone"))
        {
            dropZone = GameObject.FindGameObjectWithTag("DropZone").gameObject;
        }
    }

    private void Update()
    {
        selector.enabled = selected;
        ItemImage.texture = Resources.Load<Texture2D>("Items/" + itemName);
    }

    public void add(string newName, GameObject newItem)
    {
        itemName = newName;
        prefabItem = newItem;
        ItemImage.texture = Resources.Load<Texture2D>("Items/" + itemName);
        newItem.SetActive(false);
    }

    public bool checkEmpty()
    {
        return itemName == "Empty";
    }

    public void drop()
    {
        if (prefabItem==null) return;
        itemName = "Empty";
        prefabItem.SetActive(true);
        if (dropZone)
        {
            prefabItem.transform.position = dropZone.transform.position;
        }
        prefabItem = null;
    }

    private void delete()
    {
        itemName = "Empty";
        prefabItem = null;
    }

    public void actionItem()
    {
        if (prefabItem == null) return;
        if (prefabItem.GetComponent<drinkItem>()!=null)
        {
            prefabItem.GetComponent<drinkItem>().drink();
            delete();
        }
        else
        {
            if (prefabItem.GetComponent<foodItem>()!=null)
            {
                prefabItem.GetComponent<foodItem>().eat();
                delete();
            }
        }
    }
}
