﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemObject : MonoBehaviour
{
    public string itemName;

    public int itemValue;

    public string itemDescription;
    
    [SerializeField] 
    protected static NetworkPlayerController player;
    
    void Start()
    {
        InvokeRepeating("RetievePlayer", 1, 1);
    }
    void RetievePlayer()
    {
        if (!player && GameObject.FindGameObjectWithTag("MainPlayer"))
        {
            player = GameObject.FindGameObjectWithTag("MainPlayer").GetComponent<NetworkPlayerController>();
            Debug.Log(GameObject.FindGameObjectWithTag("MainPlayer"));
        }
    }
    
}
