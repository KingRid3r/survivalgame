﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drinkItem : ItemObject
{

    // Start is called before the first frame update
    void Start()
    {
        itemName = "Soda";
        itemValue = 50;
        itemDescription = "Ceci est une boisson qui regenere 50 points de soif !";
    }
    
    public void drink()
    {
        Debug.Log(player);
        if (player)
        {
            player.IncreaseWater(itemValue);
        }
    }
}
